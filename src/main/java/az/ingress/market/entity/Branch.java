package az.ingress.market.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String branchname;
    @OneToOne
    @JoinColumn(name = "address_id",referencedColumnName = "id")
    Address address;
    @ManyToOne
    @JoinColumn(name = "market_id",referencedColumnName = "id")
    Market market;
}
