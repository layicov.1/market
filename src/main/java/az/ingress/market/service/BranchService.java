package az.ingress.market.service;

import az.ingress.market.dto.BranchDto;
import az.ingress.market.dto.MarketDto;
import az.ingress.market.entity.Branch;
import az.ingress.market.entity.Market;

public interface BranchService {

    String saveBranch(Branch branch);
    BranchDto getBranch(Long id);
}
