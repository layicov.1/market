package az.ingress.market.service;

import az.ingress.market.dto.MarketDto;
import az.ingress.market.entity.Market;

public interface MarketService {

    String saveMarket(Market market);
    MarketDto getMarket(Long id);
}
