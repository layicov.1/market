package az.ingress.market.service.impl;

import az.ingress.market.dto.MarketDto;
import az.ingress.market.entity.Market;
import az.ingress.market.repository.MarketRepository;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketServiceimpl implements MarketService {
    public final MarketRepository marketRepository;
    public final ModelMapper modelMapper;

    @Override
    public String saveMarket(Market market) {
        marketRepository.save(market);
        return market.getMarketName();
    }

    @Override
    public MarketDto getMarket(Long id) {
        return modelMapper.map(marketRepository.findById(id).get(),MarketDto.class);
    }
}
