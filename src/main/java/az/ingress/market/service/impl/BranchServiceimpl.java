package az.ingress.market.service.impl;

import az.ingress.market.dto.AddressDto;
import az.ingress.market.dto.BranchDto;
import az.ingress.market.entity.Address;
import az.ingress.market.entity.Branch;
import az.ingress.market.repository.AddressRepository;
import az.ingress.market.repository.BranchRepository;
import az.ingress.market.service.AddressService;
import az.ingress.market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BranchServiceimpl implements BranchService {
    public final BranchRepository branchRepository;
    public final ModelMapper modelMapper;

    @Override
    public String saveBranch(Branch branch) {
        branchRepository.save(branch);
        return branch.getBranchname();
    }

    @Override
    public BranchDto getBranch(Long id) {
        return modelMapper.map(branchRepository.findById(id).get(), BranchDto.class);
    }
}
