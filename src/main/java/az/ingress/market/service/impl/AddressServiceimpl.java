package az.ingress.market.service.impl;

import az.ingress.market.dto.AddressDto;
import az.ingress.market.dto.MarketDto;
import az.ingress.market.entity.Address;
import az.ingress.market.entity.Market;
import az.ingress.market.repository.AddressRepository;
import az.ingress.market.repository.MarketRepository;
import az.ingress.market.service.AddressService;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressServiceimpl implements AddressService {
    public final AddressRepository addressRepository;
    public final ModelMapper modelMapper;

    @Override
    public String saveAddress(Address address) {
        addressRepository.save(address);
        return address.getAddressName();
    }

    @Override
    public AddressDto getAddress(Long id) {
        return modelMapper.map(addressRepository.findById(id).get(),AddressDto.class);
    }
}
