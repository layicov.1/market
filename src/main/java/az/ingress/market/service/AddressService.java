package az.ingress.market.service;


import az.ingress.market.dto.AddressDto;
import az.ingress.market.entity.Address;

public interface AddressService {
    String saveAddress(Address address);
    AddressDto getAddress(Long id);
}
