package az.ingress.market.controller;

import az.ingress.market.dto.MarketDto;
import az.ingress.market.entity.Market;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {
    public final MarketService marketService;
    @GetMapping("/{id}")
    MarketDto getMarket(@PathVariable Long id){
        return marketService.getMarket(id);
    }

    @PostMapping("/newmarket")
    String saveMarket(@RequestBody Market market){
        return marketService.saveMarket(market);
    }

}
