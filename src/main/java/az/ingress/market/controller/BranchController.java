package az.ingress.market.controller;

import az.ingress.market.dto.AddressDto;
import az.ingress.market.dto.BranchDto;
import az.ingress.market.entity.Address;
import az.ingress.market.entity.Branch;
import az.ingress.market.service.AddressService;
import az.ingress.market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    public final BranchService branchService;
    @GetMapping("/{id}")
    BranchDto getBranch(@PathVariable Long id){
        return branchService.getBranch(id);
    }

    @PostMapping("/newbranch")
    String saveBranch(@RequestBody Branch branch){
        return branchService.saveBranch(branch);
    }

}
