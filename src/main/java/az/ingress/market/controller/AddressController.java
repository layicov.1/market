package az.ingress.market.controller;

import az.ingress.market.dto.AddressDto;
import az.ingress.market.dto.MarketDto;
import az.ingress.market.entity.Address;
import az.ingress.market.entity.Market;
import az.ingress.market.service.AddressService;
import az.ingress.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {
    public final AddressService addressService;
    @GetMapping("/{id}")
    AddressDto getAddress( @PathVariable Long id){
        return addressService.getAddress(id);
    }

    @PostMapping("/newaddress")
    String saveMarket(@RequestBody Address address){
        return addressService.saveAddress(address);
    }

}
