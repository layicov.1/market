package az.ingress.market.dto;

import az.ingress.market.entity.Address;
import az.ingress.market.entity.Market;
import lombok.*;
import lombok.experimental.FieldDefaults;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchDto {
    String branchname;
    Address address;
    Market market;
}
