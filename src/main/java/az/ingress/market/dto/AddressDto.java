package az.ingress.market.dto;


import az.ingress.market.entity.Branch;
import lombok.*;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddressDto {
    String addressName;
}

